app.controller('blockBuilderCtrl', function($scope , ProService , $location) {
    var ls=localStorage;
    $scope.user = ls.getItem("user") || ProService.get("user");
    $scope.logOut = function(){
        ls.removeItem("user");
        ProService.set("user","");
        $location.path("/");
    }
    $scope.load = function() {
        var $ = angular.element;
        $(document).ready(function () {
            var countObj = {
                linkCount:0,
                paraCount:0,
                picCount:0,
                headCount:0
            }

            function hideBackGround() {
                $(".background").hide()
            }
            function showBackground() {
                $(".background").show()
            }
            $("#link").draggable({
                cancel: false,
                cursor: 'move',
                helper: linkHelper,
                stop:function () {
                    hideBackGround();
                    addLink();
                }
            });

            $("#para").draggable({
                cancel: false,
                cursor: 'move',
                helper: paraHelper,
                stop:function () {
                    hideBackGround();
                    addPara();
                }
            });

            $("#pic").draggable({
                cancel: false,
                cursor: 'move',
                helper: picHelper,
                stop:function () {
                    hideBackGround();
                    addPic();
                }
            });
            $("#heading").draggable({
                cancel: false,
                cursor: 'move',
                helper: headHelper,
                stop:function () {
                    hideBackGround();
                    addHeading();
                }
            });

            // $("#componentHolder").droppable({
            //
            // });
            function linkHelper() {
                return '<div class="helper"><a>Your Link Here</a></div>'
            }
            function paraHelper() {
                return '<div class="helper"><p>Your Paragraph Here</p></div>'
            }
            function picHelper() {
                return '<div class="helper"><img src="https://dummyimage.com/300x300/000/fff&text=Dummy+Image"></div>'
            }
            function headHelper() {
                return '<div class="helper"><h1>Your Heading Here</h1></div>'
            }



            function addLink() {
                var idCountHere =  ++countObj.linkCount;
                var id="link"+idCountHere;
                var linkHtml  = $("<a>", {
                        id: id, 
                        href: "#",
                        text:"Your Link Here "+idCountHere,
                        target:"_self"
                    })
                .hover(function(){
                    onHoverIn(this, linkSettingClick);
                })

                $("#htmlContainer").append(linkHtml);
                var wrapinkElament = $("<div>", {
                        id: id+"-masterWrap",
                        class: "classOutWrap"
                    });
                $("#"+id).wrap(wrapinkElament);
            }

            function addPara() {
                var idCountHere =  ++countObj.paraCount;
                var id="para"+idCountHere;
                var paraHtml  = $("<p>", {
                        id: id,
                        text:"Your Paragraph Here " +idCountHere+"(Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sit amet quam risus. Vestibulum eu ultrices nibh. Nullam ullamcorper nec lacus feugiat dictum. Nullam eu elementum ante. Proin auctor rhoncus diam a consequat.)"
                    })
                .hover(function(){
                    onHoverIn(this, paraSettingClick);
                })
                $("#htmlContainer").append(paraHtml);
                var wrapinkElament = $("<div>", {
                        id: id+"-masterWrap",
                        class: "classOutWrap"
                    });
                $("#"+id).wrap(wrapinkElament);
            }

            function addPic() {
                var idCountHere =  ++countObj.picCount;
                var id="pic"+idCountHere;
                var picHtml  = $("<img>", {
                        id: id,
                        src:"https://dummyimage.com/300x300/000/fff&text=Dummy+Image"
                       }).hover(function(){onHoverIn(this , picSettingClick);});

                $("#htmlContainer").append(picHtml);
                var wrapinkElament = $("<div>", {
                        id: id+"-masterWrap",
                        class: "classOutWrap"
                    });
                $("#"+id).wrap(wrapinkElament);
            }

            function addHeading() {
                var idCountHere =  ++countObj.headCount;
                var id="head"+idCountHere;
                var headHtml  = $("<h1>", {
                        id: id,
                        text:"Your heading here "+idCountHere
                       }).hover(function(){onHoverIn(this , headSettingClick);});

                $("#htmlContainer").append(headHtml);
                var wrapinkElament = $("<div>", {
                        id: id+"-masterWrap",
                        class: "classOutWrap"
                    });
                $("#"+id).wrap(wrapinkElament);
            }

            function onHoverIn(self, settingFunction){
                console.log(this);
                var wrapedValue = $(self).attr("data-wrapped") ==="true"?true:false;
                if(!wrapedValue){
                    var wrapDiv = $("<div>", {
                        class:"wrapperDiv",
                        id : self.id+'-wrapper'
                    });
                    var moveButton =  $("<button>", {
                        class:"btn btn-xs btn-success pull-right margin2",
                        id: self.id+'-drag',
                        click:moveClick,
                        html:'<span class="glyphicon  glyphicon-move"></span>',
                        css:{"cursor":"move"}
                    });
                    var settingButton =  $("<button>", {
                        class:"btn btn-xs btn-warning pull-right margin2",
                        id: self.id+'-setting',
                        click:settingFunction,
                        html:'<span class="glyphicon glyphicon-cog"></span>',
                        css:{"cursor":"move"}
                    });
                    var closeButton =  $("<button>", {
                        class:"remove btn btn-danger btn-xs pull-right margin2",
                        id: self.id+'-close',
                        html:'<span class="glyphicon glyphicon-remove"></span>',
                        click:closeClick
                    });
                    var resizeButton =  $("<button>", {
                        class:"btn btn-primary btn-xs pull-right margin2",
                        id: self.id+'-resize',
                        html:'<span class="glyphicon glyphicon-resize-full"></span>',
                        click:resizeClick,
                        css:{"cursor":"e-resize"}
                    });
                    var preDiv =  $("<div>", {
                        class:'insideWrap',
                        id:self.id+'-insideWrapper'
                    }).append(closeButton , settingButton , moveButton , resizeButton);


                    $("#"+self.id+"-masterWrap").wrap(wrapDiv);
                    $("#"+self.id+"-masterWrap").before(preDiv);
                    $(self).attr("data-wrapped", true);

                }
                // console.log(this);
            }
            
            function linkSettingClick(){
                var elementId = this.id.split("-")[0];
                var targetElement = $("#"+elementId);
                $('#linkSettingModal').modal('show');
                $("#link-text").val(targetElement.text());
                $("#link-href").val(targetElement.attr("href"));
                $("#link-target").val(targetElement.attr("target"));
                $("#saveEditLink").one("click" , function(){reflectLinkChanges(elementId , '#linkSettingModal')});
            }
            
            
            function paraSettingClick(){
                var elementId = this.id.split("-")[0];
                var targetElement = $("#"+elementId);
                $('#paraSettingModal').modal('show');
                $("#para-text").val(targetElement.text());
                $("#saveEditPara").one("click" , function(){reflecParaChanges(elementId , '#paraSettingModal')});
            }
            function picSettingClick(){
                var elementId = this.id.split("-")[0];
                var targetElement = $("#"+elementId);
                $('#picSettingModal').modal('show');
                $("#pic-src").val(targetElement.attr("src"));
                $("#saveEditPic").one("click" , function(){reflecPicChanges(elementId , '#picSettingModal')});
            }
            function headSettingClick(){
                var elementId = this.id.split("-")[0];
                var targetElement = $("#"+elementId);
                $('#headSettingModal').modal('show');
                $("#head-type").val(targetElement.prop("tagName").toLowerCase());
                $("#head-text").val(targetElement.text());
                $("#saveEditHead").one("click" , function(){reflectHeadChanges(elementId , '#headSettingModal')});
            }


            function closeClick (){
                var elementId = this.id.split("-")[0];
                $('#closeModal').modal('show');
                $("#deleteElement").one("click" , function(){deleteElement(elementId , "#closeModal")});
                console.log(this);
            }
            function moveClick(){
                console.log(this);
                var wrapperId = this.id.split("-")[0]+"-wrapper";
                $("#htmlContainer").sortable({cancel: false});
                $("#htmlContainer").css({"cursor":"move"});

            }
            function resizeClick(){
                var elementId = this.id.split("-")[0];
                var targetElement = $("#"+elementId);
                $("#"+elementId+"-wrapper").css({"border-color":"red"});
                var elementToResize = "#"+elementId+"-masterWrap";
                $(elementToResize).resizable({
                    containment: "#htmlContainer",
                    ghost: true,
                    animate: true,
                    stop:function(){
                        $("#"+elementId+"-wrapper").css({"border-color":"#1c98ff"});

                    }
                });
            }

            function deleteElement(id , modalSelector){
                selector =$("#"+id);
                $("#"+id+"-insideWrapper").remove();
                selector.unwrap(".classOutWrap");
                selector.unwrap("#"+id+"-wrapper");
                selector.remove();
                closeModalBySelector(modalSelector);
            }

            function reflectLinkChanges(id , modalSelector){
                selector =$("#"+id);
                console.log("selector" , selector);
                console.log("selector id" , id);
                selector.text($("#link-text").val());
                selector.attr("href" , $("#link-href").val());
                selector.attr("target" , $("#link-target").val());
                selector.attr("data-wrapped", false);
                $("#"+id+"-insideWrapper").remove();
                $("#"+id+"-masterWrap").unwrap("#"+id+"-wrapper");
                closeModalBySelector(modalSelector);
                selector.attr("data-wrapped", false);

            }
            function reflecParaChanges(id , modalSelector){
                selector =$("#"+id);
                console.log("selector" , selector);
                console.log("selector id" , id);
                selector.text($("#para-text").val());
                $("#"+id+"-insideWrapper").remove();
                $("#"+id+"-masterWrap").unwrap("#"+id+"-wrapper");
                closeModalBySelector(modalSelector);
                selector.attr("data-wrapped", false);

            }
            function reflecPicChanges(id , modalSelector){
                selector =$("#"+id);
                console.log("selector" , selector);
                console.log("selector id" , id);
                selector.attr("src" , $("#pic-src").val());
                $("#"+id+"-insideWrapper").remove();
                $("#"+id+"-masterWrap").unwrap("#"+id+"-wrapper");
                closeModalBySelector(modalSelector);
                selector.attr("data-wrapped", false);

            }
            function reflectHeadChanges(id , modalSelector){
                selector =$("#"+id);
                console.log("selector" , selector);
                console.log("selector id" , id);
                // selector.text($("#head-text").val());
                var composedHtml=$("<"+ $("#head-type").val()+">", {
                        id: id,
                        text:$("#head-text").val()
                    });
                console.log(composedHtml);
                
                $("#"+id+"-insideWrapper").remove();
                $("#"+id+"-masterWrap").unwrap("#"+id+"-wrapper");
                composedHtml.hover(function(){ onHoverIn(this , headSettingClick);})
                selector.replaceWith(composedHtml);
                closeModalBySelector(modalSelector);
                selector.attr("data-wrapped", false);

            }
            function closeModalBySelector(sel){
                $(sel).modal("hide");
            }
            $("#generatrHtml").click(generateHtml);
            function generateHtml (){
                var allCountKeys = Object.keys(countObj);
                var max = getMaxCount(countObj);
                //delete all wrappers
                while(max){
                    var link = "#link"+max;
                    var para = "#para"+max;
                    var pic =  "#pic"+max;
                    var head = "#head"+max;
                    //remove inside wrapper
                    $(link+"-insideWrapper").remove();
                    $(para+"-insideWrapper").remove();
                    $(pic+"-insideWrapper").remove();
                    $(head+"-insideWrapper").remove();

                    //removing outside wrapper
                    $(link+"-masterWrap").unwrap(link+"-wrapper");
                    $(para+"-masterWrap").unwrap(para+"-wrapper");
                    $(pic+"-masterWrap").unwrap(pic+"-wrapper");
                    $(head+"-masterWrap").unwrap(head+"-wrapper");
                    max--;
                }
                $(".background").remove();
                var generatedHtml = $("#htmlContainer").html();
                $('#generateHtmlModal').modal('show');
                $("#html-gen").val(generatedHtml);
            }
            function getMaxCount(obj){
                var allCountKeys = Object.keys(obj);
                return allCountKeys.reduce(function(acc, pre){
                    acc = obj[pre] > acc ? obj[pre] : acc;
                    return acc;
                } , 0)
            }

        });
    };
    $scope.load();
})