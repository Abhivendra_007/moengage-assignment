app.controller('loginCtrl', function($scope , $location , ProService) {
    var ls=localStorage;
    if(ls.getItem("user") || ProService.get("user")){
        $location.path('/blockBuilder');
    }
    $scope.userData = {
        user:"",
        email:"",
        pass:""
    };
    $scope.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
    $scope.passFormat = /^[a-zA-Z0-9.]{3,15}$/;
    $scope.login = function(){
        if( $scope.userData.email === "demo@getmein.com" && $scope.userData.pass === "demo"){
            $scope.userData.user = $scope.userData.email.split("@")[0];
            ls.setItem("user" , $scope.userData.user)
            ProService.set("user" , $scope.userData.user)
            $location.path('/blockBuilder');
        }
        else{
            alert("Wrong details!")
        }
    }

})