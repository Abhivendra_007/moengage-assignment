var app = angular.module('project', ['ngRoute']);
 
app.factory('ProService', function () {
    var info = {};
    return {
        set: function (key, data) {
            info[key] = data;
        },
        get: function (key) {
            return info[key];
        }
    }
})

.config(function($routeProvider) {
  $routeProvider
    .when('/', {
      controller:'loginCtrl',
      templateUrl:'app/views/login.html'
    })
    .when('/blockBuilder', {
      controller:'blockBuilderCtrl',
      templateUrl:'app/views/blockBuilder.html'
    })
})
 

 
